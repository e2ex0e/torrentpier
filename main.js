
	$(document).ready( function() {

		(function sendAjaxResponse() {

			var parse_data = getElementsLink();
				// main_container = $('<div class="main_container"></div>'),
				// img_array = {};

			if( parse_data.error !== '1' ) {


			var parsedContent = {

				contents : {},

				setContent: function (id, title, topicLink, imageLink, topicContent) {

					this.contents[id] = {
						id: id,
						title: title,
					    topicLink: topicLink,
					    imageLink: imageLink,
					    topicContent: topicContent
					};

					// console.log(this);

					// var htmlElements = '';
					// for(item in this.contents) {
					// 	// console.log(this.contents[item]);
					// 	htmlElements += this.createHtmlElementTemplate(this.contents[item]);
					// }
					// console.log(htmlElements);

				},

				// getTopicContent: function (id){

				// 	return this.contents[id].content;
				// },

				// getImageLink: function (id){

				// 	return this.contents[id].image;
				// },

				returnHtmlElements: function() {
					// console.log(this);
					var htmlElements = '';
					for(var item in this.contents) {
						// console.log(this.contents[item]);
						htmlElements += this.createHtmlElementTemplate();
					}
					return htmlElements;

				},

				createHtmlElementTemplate: function(item) {
					// console.log(item)
					return  `<div class="topic_wrap" id="${item.id}">
									<div class="cover">
										<img src="${item.imageLink}" />
										<div class="img_pos_button">&infin;</div>
									</div>
									<div class="topic_info">
										<a target="_blank" href="${item.topicLink}">${item.title}</a>
									</div>
									<div class="download">
										<div class="dl" attr-id="${item.id}">Скачать</a>
									</div>
								</div>`;
				},

				returnHtmlTemplate: function() {
					var htmlElements = this.returnHtmlElements();
					// console.log(htmlElements);
					return `<div class="main_container">${htmlElements}</div>`;

				}

			};

				$.each(parse_data, function(index, value) {
					var topicLink = value[1];
					var title = value[0];
					var id = topicLink.split('viewtopic.php?t=')[1];
					// var img_links = {};
					// var cover	= createEl('cover'),
					// 	seed_info = createEl('seed_info'),
					// 	topic_status = createEl('topic_status'),
					// 	topic_info = createEl('topic_info'),
					// 	topic_date = createEl('topic_date'),
					// 	topic_wrap = createEl('topic_wrap', id),
					// 	img_pos_button = createEl('img_pos_button'),
					// 	download_link  = createEl('download');


					 $.ajax({
						url: topicLink,
						dataType : "html",
						success: function (data) {

							var	topicContent = $(data).find('#topic_main').find('[id^="post_"]').first().find('.post_body');
							var imageLink = $(topicContent).find('var.postImg[title*="jp"]').attr('title');

							// var image = new Image();
							// image.src = attr;
							// var imageSize = image.src;

							// var imageSized = '';
							// image.addEventListener('load', function(imageSized) {
							// 	 imageSized = this.naturalWidth;
							// });

							// $(cover).append('<img src="'+ attr +'" />');
							// console.log(parsedContent);
							parsedContent.setContent(id, title, topicLink, imageLink, $(topicContent).text());

						}
					});
					// console.log(parsedContent.contents)
					// $('.main_container').append(topic_wrap);


					// $(topic_info).html('<a target="_blank" href="' + href + '">' + value[0] +'</a>');

					// $(download_link).html('<div class="dl" attr-id="'+ key +'">Скачать</a>')

					// $(cover).append($(img_pos_button).html('&infin;'));

					// $('#' + key).append(cover).append(topic_info).append(download_link);


				});

				if(window.location.pathname === '/forum/tracker.php') {
					console.log(parsedContent.returnHtmlTemplate())
					$('#search-results').html(parsedContent.returnHtmlTemplate());
				} else {
					$('.forum').remove();
					console.log(parsedContent.returnHtmlTemplate())
					$('.forumline').after(parsedContent.returnHtmlTemplate());
				}

			}

		})();

		$(document).on('click', '.download .dl', function() {
			var key = $(this).attr('attr-id');
			document.cookie = "bb_dl=" + key;
			window.location = '/forum/dl.php?t=' + key;
		});


		$(document).on('click', '.img_pos_button', function() {

			$(this).addClass('active_button');
			$(this).parent('.cover').find('img').css({'right': 0, 'left': 'auto'});

		});

		$(document).on('click', '.active_button', function() {

			$(this).removeClass('active_button');
			$(this).parent('.cover').find('img').css({'left': 0, 'right': 'auto'});

		});

		// function createEl(classname, id) {

		// 	return $('<div />', { class: classname, id: id });

		// }

		/*

			Get all links from the tread on page and search the results pages

		*/

		function getElementsLink() {
			var parse_data = {};

			/*
				If gotten url equal search-result page, if else gotten url equal treads result page

		 	*/

			if(window.location.pathname === '/forum/tracker.php') {

				$('tr.tCenter').each(function() {
					var item_array = [],
							href    	 = $(this).find('td:nth-child(4) a').attr('href'),
							item_id 	 = href.split('viewtopic.php?t=')[1],
							title 		 = $(this).find('td:nth-child(4) a').text();

					if(href !== undefined) {

						item_array.push(title);
						item_array.push(href);

						parse_data[item_id] = item_array;
					} else {
						parse_data['error'] = 1;
					}

				});

			} else {

				/*
					Get a links from the tread results
				*/

				$('tr[id^=tr]').each(function() {

					var item_array = [],
							item_id = $(this).attr('id'),
							links = $(this).find('td:nth-child(2) div.torTopic a[id*=tt]'),
							title = $(links).text(),
							href = $(links).attr('href');

					if(href !== undefined) {
						var currentId = href.split('viewtopic.php?t=')[1];

						var filterIdList = [
							'2299047','982825','980477','980475',
							'1177684','1068737','2301497','1248592',
							'2149033','1549962','1001623','1505277',
							'982827','2299048','2197387','2299049',
							'2290060','1788220','1943110','2301511',
							'1139825','1070156','2254005','2119557',
							'1139783','1847412','1509026',"1278172",
							"2101174","998781","1033780","997512"
						];

						if($.inArray(currentId, filterIdList) === -1) {

							item_array.push(title);
							item_array.push(href);

							parse_data[item_id] = item_array;

						}

					} else {
						parse_data['error'] = 1;
					}


				});
			}
			return parse_data;
		}

		getElementsLink();


	});