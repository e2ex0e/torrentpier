// ==UserScript==
// @name        custom_js_torrentpier
// @namespace   js
// @include     */forum/viewforum.php?f=*
// @include     */forum/tracker.php*
// @include		*/forum/search.php*
// @require     https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js
// @resource    mainCss https://bitbucket.org/e2ex0e/torrentpier/raw/279d91b9e3e39b1cbf778c9d09e0e3768108c58d/main.css
// @version     1
// @grant       GM_addStyle
// @grant       GM_getResourceText
// ==/UserScript==



var mainCss = GM_getResourceText("mainCss");
GM_addStyle(mainCss);

this.$ = this.jQuery = jQuery.noConflict(true);

$(document).ready( function() {

	(function sendAjaxResponse() {

		var parse_data = getElementsLink();

		if( parse_data.error !== 1 ) {

			if(window.location.pathname === '/forum/tracker.php') {
				$('#search-results').html('<div class="main_container"></div>');
			} else {
				$('.forum').remove();
				$('.forumline').after('<div class="main_container"></div>');
			}

			var Elements = {

				getElement: function (id, title, topicLink, imageLink) {
					return this.createElementWrapper(id, title, topicLink, imageLink);
				},

				createElementWrapper: function(id, title, topicLink, imageLink) {
					return  `<div class="topic_wrap" id="${id}">
								<div class="cover">
									<img src="${imageLink}" />
									<div class="img_pos_button">&infin;</div>
								</div>
								<div class="topic_info">
									<a target="_blank" href="${topicLink}">${title}</a>
								</div>
								<div class="download">
									<a class="dl" attr-id="${id}">Скачать</a>
								</div>
							</div>`;
				}

			};

			$.each(parse_data, function(index, value) {
				var topicLink = value[1];
				var title = value[0];
				var id = topicLink.split('viewtopic.php?t=')[1];
				
				  	$.ajax({
						url: topicLink,
						dataType : "html",
						success: function (data) {
							
							var topicContent = $(data).find('#topic_main').find('[id^="post_"]').first().find('.post_body');
							var imageLink = $(topicContent).find('var.postImg[title*="jp"]').attr('title');

							var elementTemplate = Elements.getElement(id, title, topicLink, imageLink, $(topicContent).text());
							$('.main_container').append(elementTemplate);
						}
					});
				
			});
		}

	})();

	$(document).on('click', '.download .dl', function() {
		var key = $(this).attr('attr-id');
		document.cookie = "bb_dl=" + key;
		window.location = '/forum/dl.php?t=' + key;
	});


	$(document).on('click', '.img_pos_button', function() {

		$(this).addClass('active_button');
		$(this).parent('.cover').find('img').css({'right': 0, 'left': 'auto'});

	});

	$(document).on('click', '.active_button', function() {

		$(this).removeClass('active_button');
		$(this).parent('.cover').find('img').css({'left': 0, 'right': 'auto'});

	});

	/*

		Get all links from the tread on page and search the results pages

	*/

	function getElementsLink() {
		var parse_data = {};

		/*
			If gotten url equal search-result page, if else gotten url equal treads result page

	 	*/

		if(window.location.pathname === '/forum/tracker.php') {

			$('tr.tCenter').each(function() {
				var item_array = [],
					href    = $(this).find('td:nth-child(4) a').attr('href'),
					item_id = href.split('viewtopic.php?t=')[1],
					title 	= $(this).find('td:nth-child(4) a').text();

				if(href !== undefined) {
					item_array.push(title);
					item_array.push(href);

					parse_data[item_id] = item_array;
				} else {
					parse_data['error'] = 1;
				}

			});

		} else {

			/*
				Get a links from the tread results
			*/

			$('tr[id^=tr]').each(function() {

				var item_array = [],
						item_id = $(this).attr('id'),
						links = $(this).find('td:nth-child(2) div.torTopic a[id*=tt]'),
						title = $(links).text(),
						href = $(links).attr('href');

				if(href !== undefined) {
					var currentId = href.split('viewtopic.php?t=')[1];

					var filterIdList = [
						'2299047','982825','980477','980475',
						'1177684','1068737','2301497','1248592',
						'2149033','1549962','1001623','1505277',
						'982827','2299048','2197387','2299049',
						'2290060','1788220','1943110','2301511',
						'1139825','1070156','2254005','2119557',
						'1139783','1847412','1509026',"1278172",
						"2101174","998781","1033780","997512"
					];

					if($.inArray(currentId, filterIdList) === -1) {

						item_array.push(title);
						item_array.push(href);

						parse_data[item_id] = item_array;

					}

				} else {
					parse_data['error'] = 1;
				}


			});
		}
		return parse_data;
	}

	getElementsLink();


});